% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
%boxit,        % check whether paper is inside correct margins
%titlepage,    % separate title page
%refpage       % separate references
%biblatex,     % biblatex is used
keeplastbox,   % flushend option: not to un-indent last line in References
%nospread,     % flushend option: do not fill with whitespace to balance columns
%hyphens,      % allow \url to hyphenate at "-" (hyphens)
%xetex,        % use XeLaTeX to process the file
%luatex,       % use LuaLaTeX to process the file
]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e,dirtytalk} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
\ifboolexpr{bool{xetex}}
{\renewcommand{\Gin@extensions}{.pdf,%
		.png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
		.eps,.ps,%
}}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
{}                                      % input encoding is utf8 by default
{\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
{%
	\addbibresource{jacow-test.bib}
	\addbibresource{report.bib}
}{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}

	\title{SKA Tango Operator}

	\author{Di Carlo M.\thanks{matteo.dicarlo@inaf.it}, Dolci M., INAF Osservatorio Astronomico d'Abruzzo, Teramo, Italy \\
		Harding P., U. Yilmaz, SKA Observatory, Macclesfield, UK \\
		Osório P., Atlar Innovation, Portugal \\
		Morgado J. B., CICGE, Faculdade de Ciências da Universidade do Porto, Portugal \\
		Paulo M., Critical Software Portugal}

	\maketitle

	%
	\begin{abstract}
		The Square Kilometre Array (SKA) is an international effort to build two radio interferometers in South Africa and Australia, forming one Observatory monitored and controlled from global headquarters (GHQ) based in the United Kingdom at Jodrell Bank. The software for the monitoring and control system is developed based on the \emph{TANGO-controls} framework, which provide a distributed architecture for driving software and hardware using \emph{CORBA} distributed objects that represent devices that communicate with \emph{ZeroMQ} events internally. This system runs in a containerised environment managed by \emph{Kubernetes} (k8s). k8s provides primitive resource types for the abstract management of compute, network and storage, as well as a comprehensive set of APIs for customising all aspects of cluster behaviour. These capabilities are encapsulated in a framework (Operator SDK) which enables the creation of higher order resources types assembled out of the k8s primitives (\verb|Pods|, \verb|Services|, \verb|PersistentVolumes|), so that abstract resources can be managed as first class citizens within k8s. These methods of resource assembly and management have proven useful for reconciling some of the differences between the TANGO world and that of Cloud Native computing, where the use of Custom Resource Definitions (CRD) (i.e., Device Server and DatabaseDS) and a supporting Operator developed in the k8s framework has given rise to better usage of TANGO-controls in k8s. Keywords: kubernetes, controller, tango, operator SDK
	\end{abstract}


	\section{Introduction}
	The Square Kilometre Array (SKA) project has selected a software framework for the monitoring and control system called \emph{TANGO-controls}\cite{tango-controls}, a distributed middleware for driving software and hardware using \emph{CORBA}\cite{corba} (Common Object Request Broker Architecture) distributed objects that represent devices that communicate with \emph{ZeroMQ}\cite{zeromq} events internally. The entire system runs on a containerised environment managed by \emph{Kubernetes} (k8s)\cite{kubernetes} with \emph{Helm}\cite{helm} for packaging and deploying SKA software. In k8s, all deployment elements are resources abstracted away from the underlying infrastructure implementation. For example, a \verb|Service| (network configuration), \verb|PersistentVolume| (file-system type storage) or \verb|Pod| (the smallest deployable unit of computing, consisting of containers). The resources reside in a cluster (a set of connected machines) and share network, storage, computing power and other resources. Namespaces in k8s create a logical separation of resources within a shared multi-tenant environment. A Namespace enforces a separate network and set of access rights, enabling a virtual private space for contained deployment. Fundamentally, k8s uses
	a declarative “model” of operation that drives the system towards the desired state described by user manifests, with various controller components managing the lifecycle of the associated resources.
	Helm provides the concept of a chart which is a recipe to deploy multiple k8s resources (i.e., containers, storage, networking components, etc...) required for an application to run. The resources are created using templates so that the chart can adapt generic configurations to different environments (i.e., the different SKA datacentres). The SKA deployment practices include the heavy use of standardised Makefiles (i.e., for building container images, for testing, for the deployment of a chart, etc.) and \emph{Gitlab}\cite{gitlab} for the CICD (continuous integration continuous deployment) practices\cite{SKA-CICD},

	\section{Setting up a TANGO Device server in Kubernetes}
	The TANGO-controls framework is middleware for connecting software processes, mainly based on the CORBA standard. The CORBA standard defines how to expose the procedures of an object within a software process with the RPC protocol (Remote Procedure Call). TANGO extends the definition of an object with the concept of a \verb|Device| that represents a real or virtual device to control. This exposes commands (procedures), and attributes (i.e., state) allowing both synchronous and asynchronous communication with events generated from attributes. The software process is called Device Server. Fig.\ref{fig:tangodatamodel} shows a module view of the aforementioned framework.

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=1\columnwidth]{SimplifiedDataModel}
		\caption{TANGO-controls simplified data model.}
		\label{fig:tangodatamodel}
	\end{figure}

	Given the TANGO-controls framework, for every device server, the steps for setting up a Device Server are the following:
	\begin{itemize}
		\item Deploy the code into a node (i.e., Host, VM, container, etc...);
		\item Configure the application before running (i.e., set environment variables, configuration files, etc...);
		\item Configure any needed resources like persistent storage or network aspects;
		\item Declare the device(s) in the TANGO database of devices;
		\item Set any needed properties in the TANGO database of devices (i.e., polling definition for events, etc...);
		\item Start devices in order (if there are dependencies, wait for each of them to be fulfilled before proceeding).
	\end{itemize}

	Without container orchestration, the automation of the above steps can be complex, depending on the software automation tool used.
	With the help of k8s, it is possible to package the TANGO-controls framework into a set of base container images so that the final product is a containerised application that will run in a container orchestrator. In SKA, the repository \emph{ska-tango-images}\cite{ska-tango-images} contains the definitions of all TANGO-controls components in a set of container images together with two helm charts: \emph{ska-tango-base} and \emph{ska-tango-util}. The first one defines the basic TANGO ecosystem -- consisting mainly of the TANGO database -- while the second one is a helm library chart which helps developers define TANGO device servers through configurable template macros. Fig. \ref{fig:declarativeds} shows how to define a device server using the ska-tango-util helm chart library -- please note that only a partial set of parameters are shown.

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=1\columnwidth]{declarative-ds}
		\caption{Declarative definition of a device server.}
		\label{fig:declarativeds}
	\end{figure}

	The following code shows an example of a device server containing two instances (basically two processes) starting from the same container image.

	\begin{verbatim}
		name: timer-dev-server
		function: timer
		domain: timer-app
		instances: ["counters", "timer"]
		image:
		  registry: artefact.skao.int
		  image: ska-tango-examples
	  	  tag: 0.4.24
		  pullPolicy: IfNotPresent
		entrypoints:
		- name: "Timer.Timer"
		  path: "/app/src/Timer.py"
		- name: "Counter.Counter"
		  path: "/app/src/Counter.py"
		server:
		  instances:
		  - name: "counters"
		    classes:
		    - name: "Counter"
			  devices:
		   	  - name: "srv/counter/minutes"
			  - name: "srv/counter/seconds"
		  - name: "timer"
			classes:
			- name: "Timer"
			devices:
			- name: "test/timer/1"
			properties:
			- name: "LOGGING_LEVEL"
			  values:
			  - "DEBUG"
		livenessProbe:
		  initialDelaySeconds: 0
		  periodSeconds: 10
		  timeoutSeconds: 3
		  successThreshold: 1
		  failureThreshold: 3
		readinessProbe:
		  initialDelaySeconds: 0
		  periodSeconds: 10
		  timeoutSeconds: 3
		  successThreshold: 1
		  failureThreshold: 3
	\end{verbatim}

	Given the above definition, the helm chart library - ska-tango-util - will translate it into the following k8s resources:
	\begin{itemize}
		\item a k8s job for the initialization of the TANGO database;
		\item a service that exposes the application to the network;
		\item a \verb|Statefulset| (in k8s this is an object used to manage stateful applications) which contains:
		\begin{itemize}
			\item a \verb|Pod| (k8s object that groups one or more containers into a unit);
			\item one or more \verb|InitContainers| (specialized containers that run before app containers in a Pod) to resolve the device server dependencies in order to:
			\begin{itemize}
				\item wait for the configuration job to complete and
				\item wait for device dependencies.
			\end{itemize}
		\end{itemize}
	\end{itemize}

	Fig. \ref{fig:ska-tango-util-workflow} shows the workflow of the start of a device server in k8s, starting from the \verb|install| command.

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=1\columnwidth]{ska-tango-util-workflow}
		\caption{k8s ska-tango-util helm chart workflow.}
		\label{fig:ska-tango-util-workflow}
	\end{figure}

	\section{Extending K8s - The Operator pattern}
	The above solution for setting up a device server works appropriately, but it also presents some challenges. In specific, it creates extra resources in the k8s cluster (i.e., for instance the init-containers to solve dependencies) and leaves behind spent resources (i.e., job pods that have completed do not get deleted immediately). Other than those, the Crash Loop Backoff behaviour that exists in the Kubernetes cluster can slow the Device Server startup in case of multiple dependencies and -- since there's no central control on library chart selected by developers using the ska-tango-util -- an environment might end up with different versions of the k8s resource declarations.

	To solve the highlighted challenges, it is possible to use the k8s operator pattern, which \say{aims to capture the key aim of a human operator who is managing a service or set of services. Human operators who look after specific applications and services have deep knowledge of how the system ought to behave, how to deploy it, and how to react if there are problems}\cite{operator-pattern}.

	Technically speaking, the operator:
	\begin{itemize}
		\item extends the Control Plane to describe custom behaviours;
		\item uses Custom Resource Definitions (extending the API) to create new manageable resources;
		\item uses the control loop pattern (a non-terminating loop that regulates the state of a system) to reconcile the resources to their desired state.
	\end{itemize}

	\subsection{SKA TANGO Operator}
	The SKA TANGO Operator is a k8s Operator capable of managing TANGO resources (\verb|DeviceServer| and \verb|DatabaseDS|), controlling their lifecycle within the k8s native control/event loop. Specifically:
	\begin{itemize}
		\item allows for a lighter deployment, due to avoiding init-containers for dependency resolution;
		\item optimises the startup time for Device Servers, as the operator can directly tap into the TANGO environment and retrieve information on dependent devices and the TANGO host itself.
	\end{itemize}

	Together with solving the above challenges, the main rationale to introduce the operator is to help developers to think directly in terms of TANGO-controls and not in terms of k8s resources, as those are components relevant to the platform and not the application. Introducing the operator also allows automating many of the tasks a human would do to operate a TANGO resource, in a quicker and more reliable manner.
	It also facilitates the collection of custom metrics on the CRDs, giving information on the system in a TANGO domain instead of the k8s domain (i.e., Device Servers instead of \verb|StatefulSets|) therefore making TANGO components first-class citizens of k8s.

	The resources added with the SKA TANGO Operator are the \verb|databaseds.tango.tango-controls.org| and \verb|deviceservers.tango.tango-controls.org|. The workflow for these is represented in fig. \ref{fig:databaseds-workflow} and fig. \ref{fig:device-server-workflow}.

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=.7\columnwidth]{databaseds-workflow}
		\caption{DatabaseDS workflow.}
		\label{fig:databaseds-workflow}
	\end{figure}

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=.7\columnwidth]{device-server-workflow}
		\caption{Device Server workflow.}
		\label{fig:device-server-workflow}
	\end{figure}

	One of the main advantages -- together with the aforementioned aspects -- of using the SKA TANGO Operator is the ability to extract very detailed information in the form of \emph{Prometheus}\cite{prometheus} metrics and display them in \emph{Grafana}\cite{grafana}.
	Fig. \ref{fig:startup-time} shows a detailed view of a deployed application in SKA in order to extract information about the start-up time of a Device Server.
	Fig. \ref{fig:timing information} shows a more detailed view of the timing information, showing an example of how much time a specific server waits for the configuration to happen or for a particular dependency to be available.
	Fig. \ref{fig:operator stats} shows another refinement for the timing information, showing what the operator is doing for a selected device server.

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=.7\columnwidth]{startup-time}
		\caption{Device Server start up time.}
		\label{fig:startup-time}
	\end{figure}

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=.7\columnwidth]{timing information}
		\caption{Device Server detail timing information.}
		\label{fig:timing information}
	\end{figure}

	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=.7\columnwidth]{operator stats}
		\caption{Device Server detail operator statistics.}
		\label{fig:operator stats}
	\end{figure}

	\section{Conclusion}
	In this paper, we discussed an approach to make k8s directly manage the TANGO-controls framework Device Servers. This method allows for a number of optimizations, such as fast initialization time and startup reliability. The configuration of any device server is delegated to a specialised service so that less resources are used (in terms of memory, CPU, storage and even network utilization). It basically realises an abstraction of TANGO domain-specific components from the platform ones.
	It also provides very detailed information in the form of Prometheus metrics so that it is possible to present those on a Grafana dashboard.
	Furthermore, it is important to consider an aspect that too often is underestimated in the context of monitoring and control system, such as security. This is an aspect that, for now, belongs to future work, but it is worth mentioning that with technologies such as \emph{Linkerd}\cite{linkerd}, it is possible to secure the communication of the Device Servers within and between k8s clusters.

	\subsection{Linkerd}
	Linkerd is a security focused service mesh that provides a lightweight proxy that is attached as a \verb|SideCar| to Pods.  This captures in and outbound traffic and adds encryption/decryption, policy controls and traffic monitoring and logging.
	Linkerd can be used both inside a Cluster and between Clusters, and can provide an end to end security solution for the TANGO-controls hierarchy.  This can be achieved without modifications to the underlying application. Likerd, can also be used for non-TANGO traffic (as long as it uses the TCP protocol), whenever retrofitting mTLS presents a sizeable effort.

	\subsection{Zero Trust Networking and Securing Control Systems}
	Zero Trust Architecture (or a Zero Trust Security Model \cite{zta}) is the principle of not implicitly trusting interactions between users, devices and services within a networked topology, and instead implementing steps to verify all connections. This approach acknowledges the drift of the modern networked environment towards a state where the traditional controlled network perimeters have become porous due to the highly connected nature of the working environment and the variety of actors that require varying degrees of access to systems throughout any organisation.  For example: a developer could work from a public Wi-Fi in an airport lounge, publish code and deploy critical services within an Observatory over VPN. A Telescope Operator could visit colleagues in another institution, discuss and amend observation details carried out over an Eduroam connection back to the Observatory.
	With these changes to what constitutes the secure boundary of modern systems, and in order to improve the overall security of system operations, it is necessary to rethink how we can secure highly connected systems when the chances of the traditional outer network perimeter being compromised is now greatly increased.  This change in the status quo is demonstrated by recent attacks that have been carried out on research projects (recently ALMA \cite{alma-cyber-attack}, and NOIRLab \cite{noilab-cyber-attack}).
	One way of addressing these threats is to apply the principles of ZTA to key parts of the infrastructure, such as the Controls Systems, Monitoring and Logging.  By taking this approach, it will reduce the inner attack surface area, and even when a system is compromised, the ability to still retain trust in the system observability components is more likely to remain - something that is not possible in a compromised perimeter based security model.
	Linkerd is one such tool that can be used to provide a ZTA oriented inner layer of protection throughout a system landscape, that can encompass modern containerised applications as well as legacy application suites.

	\section{ACKNOWLEDGEMENTS}
	This work has been supported by the Italian Government (MEF - Ministero dell'Economia e delle Finanze, MIUR - Ministero dell'Istruzione, dell'Università e della Ricerca).

	%
	% only for "biblatex"
	%
	\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
		% "biblatex" is not used, go the "manual" way

		%\begin{thebibliography}{99}   % Use for  10-99  references
		\begin{thebibliography}{9} % Use for 1-9 references

			\bibitem{tango-controls}
			``Tango-controls framework.'' \url{https://www.tango-controls.org/}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{corba}
			``CORBA'' \url{https://www.corba.org/}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{zeromq}
			``ZeroMQ'' \url{https://zeromq.org/}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{kubernetes}
			``Kubernetes.'' \url{https://kubernetes.io/}.
			\newblock (Accessed: 31 August 2023).

			\bibitem{helm}
			``Helm.'' \url{https://helm.sh/}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{gitlab}
			``Gitlab.'' \url{https://about.gitlab.com/}.
			\newblock (Accessed: 31 August 2023).

			\bibitem{SKA-CICD}
			``Ci-cd practices at ska,'' International Society for Optics and Photonics,
			SPIE (2022).

			\bibitem{ska-tango-images}
			``Ska-tango-images repository.''
			\url{https://gitlab.com/ska-telescope/ska-tango-images}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{operator-pattern}
			``Kubernetes Operator Pattern.'' \url{https://kubernetes.io/docs/concepts/extend-kubernetes/operator/}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{prometheus}
			``Prometheus.'' \url{https://prometheus.io}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{grafana}
			``Grafana.'' \url{https://grafana.com/}.
			\newblock (Accessed: 31 August 2023).

			\bibitem{linkerd}
			``Linkerd.'' \url{https://linkerd.io/}.
			\newblock (Accessed: 1 September 2023).

			\bibitem{zta}
			``Linkerd.'' \url{https://en.wikipedia.org/wiki/Zero_trust_security_model}.
			\newblock (Accessed: 10 September 2023).

			\bibitem{alma-cyber-attack}
			``Linkerd.'' \url{https://www.eso.org/public/announcements/ann22014/}.
			\newblock (Accessed: 10 September 2023).

			\bibitem{noilab-cyber-attack}
			``Linkerd.'' \url{https://noirlab.edu/public/announcements/ann23022/}.
			\newblock (Accessed: 10 September 2023).

		\end{thebibliography}
	} % end \ifboolexpr
	%
	% for use as JACoW template the inclusion of the ANNEX parts have been commented out
	% to generate the complete documentation please remove the "%" of the next two commands
	%
	%%%\newpage

	%%%\include{annexes-A4}

\end{document}