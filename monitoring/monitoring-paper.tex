% !TeX spellcheck = en_GB
% !TeX program = lualatex
%
% v 2.3  Feb 2019   Volker RW Schaa
%		# changes in the collaboration therefore updated file "jacow-collaboration.tex"
%		# all References with DOIs have their period/full stop before the DOI (after pp. or year)
%		# in the author/affiliation block all ZIP codes in square brackets removed as it was not %         understood as optional parameter and ZIP codes had bin put in brackets
%       # References to the current IPAC are changed to "IPAC'19, Melbourne, Australia"
%       # font for ‘url’ style changed to ‘newtxtt’ as it is easier to distinguish "O" and "0"
%
\documentclass[a4paper,
%boxit,        % check whether paper is inside correct margins
%titlepage,    % separate title page
%refpage       % separate references
%biblatex,     % biblatex is used
keeplastbox,   % flushend option: not to un-indent last line in References
%nospread,     % flushend option: do not fill with whitespace to balance columns
%hyphens,      % allow \url to hyphenate at "-" (hyphens)
%xetex,        % use XeLaTeX to process the file
%luatex,       % use LuaLaTeX to process the file
]{jacow}
%
% ONLY FOR \footnote in table/tabular
%
\usepackage{pdfpages,multirow,ragged2e} %
%
% CHANGE SEQUENCE OF GRAPHICS EXTENSION TO BE EMBEDDED
% ----------------------------------------------------
% test for XeTeX where the sequence is by default eps-> pdf, jpg, png, pdf, ...
%    and the JACoW template provides JACpic2v3.eps and JACpic2v3.jpg which
%    might generates errors, therefore PNG and JPG first
%
\makeatletter%
\ifboolexpr{bool{xetex}}
{\renewcommand{\Gin@extensions}{.pdf,%
		.png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,%
		.eps,.ps,%
}}{}
\makeatother

% CHECK FOR XeTeX/LuaTeX BEFORE DEFINING AN INPUT ENCODING
% --------------------------------------------------------
%   utf8  is default for XeTeX/LuaTeX
%   utf8  in LaTeX only realises a small portion of codes
%
\ifboolexpr{bool{xetex} or bool{luatex}} % test for XeTeX/LuaTeX
{}                                      % input encoding is utf8 by default
{\usepackage[utf8]{inputenc}}           % switch to utf8

\usepackage[USenglish]{babel}

%
% if BibLaTeX is used
%
\ifboolexpr{bool{jacowbiblatex}}%
{%
	\addbibresource{jacow-test.bib}
	\addbibresource{report.bib}
}{}
\listfiles

%%
%%   Lengths for the spaces in the title
%%   \setlength\titleblockstartskip{..}  %before title, default 3pt
%%   \setlength\titleblockmiddleskip{..} %between title + author, default 1em
%%   \setlength\titleblockendskip{..}    %afterauthor, default 1em

\begin{document}
	
	\title{Monitoring the SKA infrastructure for CICD}
	
	\author{Di Carlo M.\thanks{matteo.dicarlo@inaf.it}, Dolci M., INAF Osservatorio Astronomico d'Abruzzo, Teramo, Italy \\
		U. Yilmaz, Harding P., SKA Observatory, Macclesfield, UK \\
		Osório P., Atlar Innovation, Portugal \\
		Morgado J. B., CICGE, Faculdade de Ciências da Universidade do Porto, Portugal \\
		Paulo M., Critical Software Portugal}
	\maketitle
	
	%
	\begin{abstract}
		The Square Kilometre Array (SKA) is an international effort to build two radio interferometers in South Africa and Australia, forming one Observatory monitored and controlled from global headquarters (GHQ) based in the United Kingdom at Jodrell Bank. The selected solution for monitoring the SKA CICD (continuous integration and continuous deployment) Infrastructure is Prometheus with the help of Thanos. Thanos is used for high availability, resilience, and long term storage retention for monitoring data. For data visualisation, the Grafana project emerged as an important tool for displaying data in order to make specific reasoning and debugging of particular aspect of the infrastructure in place. In this paper, the monitoring platform is presented while considering quality aspect such as performance, scalability, and data preservation. Keywords: prometheus, monitoring, thanos, grafana
	\end{abstract}
		
	
	
	\section{Introduction}
	
	The Square Kilometre Array (SKA) project has selected SAFe (Scaled Agile Framework) as an incremental and iterative development process. A specialized team -- named System Team -- is devoted to support the Continuous Integration, Continuous Deployment (CI/CD)\cite{SKA-CICD}, test automation, and the project components' quality. Building an infrastructure to support the CI/CD together with a monitoring solution, was one of the first goals of the team.
	
	The SKA infrastructure consists of a standard footprint of VPN/SSH gateway, monitoring, logging, storage and Kubernetes\cite{kubernetes} services tailored to support the GitLab\cite{gitlab} runner architecture that is shown in Fig.\ref{fig:simplified-infra}. Furthermore, this infrastructure is used to support Development and Integration testing facilities for the project's MVP (Minimum Viable Product). The selected logging solution is Elasticsearch\cite{elasticsearch}, storage is handled through Ceph\cite{ceph}, while Prometheus\cite{prometheus} handles monitoring (see the Prometheus section below), and the (central) artefact repository (CAR) is Nexus\cite{nexus}. It is important to realize that only artefacts produced by GitLab pipelines that have been triggered by a git tag (i.e. a release), are allowed to be stored on the CAR. On all other cases, GitLab's own artefact repository is used.
	The infrastructure shown in Fig.\ref{fig:simplified-infra} is replicated in multiple locations spread over different continents, with specific hardware on top of OpenStack\cite{openstack} or bare metal/virtual machine instances. Meaning, that for every infrastructure, the same components will be present -- at a different scale depending on the available resources. Some components, such as Elasticsearch and Thanos, exist on a single location, centralizing the access to information of several data centres.
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.8\columnwidth]{simplified infra}
		\caption{Simplified infrastructure}
		\label{fig:simplified-infra}
	\end{figure}
		
	\section{Prometheus}
	
	The selected monitoring solution, for every infrastructure, is Prometheus. It works in client-server architecture, where Prometheus acts as a client that reads (*scrapes* in its domain-specific language) timestamped information from multiple servers -- called targets or exporters. The data is stored on a disk in TSDB format~\cite{tsdb}. Fig.~\ref{fig:prometheus} shows a detailed diagram that illustrates the monitoring architecture on SKA. The main components of the diagram are:
	\begin{itemize}
		\item \textbf{Prometheus server}, which is composed by the scraper, the TSDB storage, an HTTP API, and Web interface for data querying;
		\item \textbf{Jobs/exporters}, where Prometheus scrapes information as a time series;
		\item \textbf{Grafana}, as a data visualization and export tool that integrates with Prometheus using PromQL -- a specific query language;
		\item \textbf{Altermanager}, that delivers alarm notifications to end-user systems;
	\end{itemize}
	Each exporter provides a time series uniquely identified by its metric name and some optional key-value pairs -- called labels. It is important to note that the exporter must give an extensive amount of information about the system that it is monitoring, usually under a single metrics endpoint.
	
	\begin{figure*}[!tbh]
		\centering
		\includegraphics*[width=\textwidth]{prometheus_thanos}
		\caption{Detailed Monitoring Architecture}
		\label{fig:prometheus}
	\end{figure*}

	Prometheus was adopted mainly due to its scalability, modifiability and optimal usage of resources -- with memory allocation optimized to use the minimum amount required -- as well as the very complete ecosystem of tools revolving around it. Because of the memory allocation optimizations, scaling is usually done vertically (i.e. adding more resources to the existing system) but it is also possible to have a federation of them, resembling horizontal scaling. This way, it is possible to have servers devoted to monitoring specific parts of the infrastructure, being a crucial aspect when dealing with multiple physical locations, and improving latency both when scraping and querying data.

	\subsection{Exporters}
	One of the most important quality of the Prometheus monitoring solution is its modifiability. This modifiability, makes it very easy to add new exporters since they are single-endpoint HTTP servers with well-structured data. The simplicity of exporter integration with the Prometheus solution is key to a consolidated monitoring platform. Aside from the extensive list of third-party exporters, it is possible to build custom ones (i.e. for an application), with multiple programming languages already having libraries to help on their development. This means we can monitor full-fledged clusters to simple parts of an application or a database, using the exact same resources and protocols.
	
	In the SKA, a set of well known exporters has been selected to be deployed -- or enabled -- on each node of the infrastructure. Most importantly, we make use of:
	\begin{itemize}
		\item \textbf{Node} exporter\cite{node_exporter};
		\item \textbf{Container runtime} metrics: enabled depending on the runtime (containerd, docker or podman);
		\item \textbf{Kubernetes}\cite{kubernetes} exporters using kube-state-metrics\cite{kube-state-metrics};
		\item \textbf{Elasticsearch} metrics\cite{elasticsearch};
		\item \textbf{Ceph} metrics\cite{ceph};
	\end{itemize}
		
	The configuration of the exporters on the Prometheus server, happens with the help of a Python script (prom-helper)\cite{ska-cicd-deploy-prometheus}. The script runs discovery queries on every node of the infrastructure inventory\cite{ansible}, creating JSON configuration files (targets in Prometheus domain-specific language) for each exporter type, including the discovered targets. Prometheus runs as a containerized application, using persistent storage for the data and configurations.
	
	\subsection{Alerts}
	
	A critical part of the monitoring solution selection is the ability to alarm the maintainers (System Team) of any problems in the infrastructure. Alertmanager and Prometheus work together to deliver detailed alarms on the multiple monitored infrastructures to communication channels. As multiple infrastructures are monitored, it is crucial that the alarm system allows for the de-duplication and grouping of alarms, as well as the capability to deliver the notifications to different communication channels. Community-developed alarming standards were adopted, as the \textit{kubernetes-mixin}\cite{kubernetes-mixin}, enabling an in-depth alarming system of open-source products. Also, some custom exporters were developed to expose metrics on custom-built Kubernetes Operators. Fig.\ref{fig:alerts} shows an example alarm notification.
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.7\columnwidth]{alerts}
		\caption{An alert message in Slack for the Mid Prototype System Integration (PSI MID) cluster}
		\label{fig:alerts}
	\end{figure}
	
	\section{Thanos}\label{thanos-section}
	Thanos is an open source project that provides global query views, high availability and long term data storage with historical querying capabilities~\cite{thanos}. Thanos is able to store data from multiple Prometheus instances, allowing queries to target multiple data-sources. It has several components targeted towards data replication, storage integration and centralised load-balanced query functions -- see: Fig.~\ref{fig:prometheus} for some highlights. Its main components and how they are used on SKA are explained below. Note that SKA is currently progressing towards implementing the architecture detailed in Fig.~\ref{fig:prometheus}.
	
	\begin{itemize}
		\item \textbf{Thanos Sidecar} is the main component. It queries Prometheus, makes it available for querying, and uploads TSDB (Time series database) blocks to Thanos' target object store. This is deployed next to each Prometheus instance.
		\item \textbf{Thanos Store Gateway} provides a store API on top of the historical data present on the object store;
		\item \textbf{Thanos Ruler} is the equivalent of Prometheus Ruler. Used to manage Thanos' rules;
		\item \textbf{Thanos Compactor} is responsible for down-sampling and data retention for efficient long term storage;
		\item \textbf{Thanos Querier} aggregates, de-duplicates the underlying data, and implements the Prometheus API;
		\item \textbf{Thanos Query Frontend} is a proxy to Thanos Queries, that improves range queries to the API by splitting and caching previous results;
	\end{itemize}

    The ability to use push or pull methods to collect Thanos Sidecar data, allows for different infrastructural and security requirements to be met, while maintaining the capability of accessing the data outside of premises. Being the SKA a globally distributed project, it is crucial to be able to access the data from multiple global locations, empowered by the replications features of the object store. Furthermore, the setup and access of user-level tools, like Grafana, to the stored data, is greatly simplified by these capabilities.

	\section{Grafana}\label{grafana-section}
	Prometheus API, uses the HTTP protocol, with a well-structured and simple format. Thus, it becomes simple to integrate with other systems using PromQL, which is particularly important for data analysis and visualization. The de facto visualization tool is Grafana~\cite{grafana}. which, by itself, can also have multiple data-sources other than Prometheus. It is remarkable the modularity and completeness of the Prometheus ecosystem as a fully-featured and open-source monitoring solution. Visualization is organized in dashboards, composed of reusable display panels. Dashboards usually target a particular source type (e.g., node metrics, kubernetes metrics), but the reusable panels can be used to build a global overview dashboard of the whole infrastructure. From an architectural point of view, it is built with a plugin architecture where a plugins can be:
	\begin{itemize}
		\item Panels (the basic visualization building block in Grafana), that transform raw data from a data-source into complex visualizations (e.g., maps, clocks, pie-charts);
		\item Data-sources that target external systems, retrieving the data from the upstream data-source, and reconciling the differences between the external and Grafana's data model;
		\item Apps that combine both data-sources and panels to visualize in a cohesive manner.
	\end{itemize}
	
	This architecture can be personalized extensively, correlating different data-sources, giving developers and testers the ability to diagnose system-wide problems. Fig.~\ref{fig:grafana-model} shows the resulting model in UML. Grafana represents an aggregation of plugins and multiple dashboards (a collection of panels). Data-sources can be Thanos or Prometheus instances that themselves collect data on several other data-sources, or directly other external, whenever historical storage is not required. In SKA, the TANGO Archiver~\cite{tangoarchiver} database or the Elasticsearch cluster are monitored this way.
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.8\columnwidth]{grafana-model}
		\caption{Grafana UML model}
		\label{fig:grafana-model}
	\end{figure}

	\section{Dashboards}
	
	The core tools and services that support the SKA infrastructure have community-developed dashboards, as it has alarming standards, displaying information that empower health and performance analysis on the entire CI/CD infrastructure. In this section we present some of the dashboard that were adopted for the various data-sources:
	Fig. \ref{fig:node-exporter-dashboard}, fig. \ref{fig:elasticsearch-dashboard} and fig. \ref{fig:docker-exporter-dashboard} shows some dashboards adopted from the Grafana community:
	\begin{itemize}
		\item \textbf{Node} exporter\cite{node-exporter-full},
		\item \textbf{Elasticsearch} cluster\cite{elasticsearch-dashboard},
		\item \textbf{Docker} containers\cite{docker-dashboard},
	\end{itemize}

	Fig. \ref{fig:k8s-dashboard} shows a dashboard provided by the kubernetes-mixin\cite{kubernetes-mixin} project.
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.8\columnwidth]{node-exporter-dashboard}
		\caption{Node exporter dashboard}
		\label{fig:node-exporter-dashboard}
	\end{figure}
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.8\columnwidth]{elasticsearch dashboard}
		\caption{Elasticsearch dashboard for cluster health}
		\label{fig:elasticsearch-dashboard}
	\end{figure}
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.8\columnwidth]{docker-exporter-dashboard}
		\caption{Docker metrics dashboard for container health}
		\label{fig:docker-exporter-dashboard}
	\end{figure}
	
	\begin{figure}[!htb]
		\centering
		\includegraphics*[width=0.8\columnwidth]{k8s-dashboard}
		\caption{Kubernetes pods dashboard per node}
		\label{fig:k8s-dashboard}
	\end{figure}

    Having alarms and visualizations on the same data, it becomes possible to track down causes for issues on pieces of the infrastructure that might not apparently relate. Application-level components may malfunction due to system-level components misbehaving, and the capability to display performance-level metrics of multiple systems, can helps shed light on root-causes.

    \section{Global view of infrastructure operations}
	The SKA has the particularity of being a globally-distributed project, as data might be needed in a different part of the world than it was collected or stored. Therefore, it becomes paramount that the monitoring system allows to deliver data across the globe, efficiently. Looking at the monitoring system as three main components - collection, storage and access/querying - the selected tools allow to distribute the workload to realize them in a global scale, maintaining high-performance on all planes.

    \begin{itemize}
		\item \textbf{Prometheus} collects metrics on several systems, locally or within public realms;
        \item \textbf{Alertmanager} delivers real-time alarm notifications on monitored systems, to multiple communication platforms;
		\item \textbf{Thanos} collect metrics from several infrastructures, consolidating the data for long-term storage. It also allows data to be replicated across locations and accessed with low latency;
        \item \textbf{Grafana} allows data to be visualized and correlated data between different systems of the same (or other) infrastructures;
	\end{itemize}

    Together these tools allow a global oversight on multiple pieces of infrastructure, while allowing for different infrastructural requirements to be met and integrated in a centralized (yet distributed) monitoring solution.
	
	\section{Conclusion and future work}
	This paper presented an overview of the tools selected by the SKA project in order to monitor the performance of the infrastructure put in place for CI/CD purposes. In specific Prometheus with Thanos and Grafana have been selected for this scope.
	The main rationale for this choice is the modularity of the several tools withing the Prometheus ecosystem (custom exporters, data-sources and visualizations), together with performance efficiency (optimal usage of memory allocation) and scalability (vertical only at the moment). Another important aspect to consider in the selection, is the outstanding community around them, keeping them active a feature-rich.

	The metrics gathered and displayed on the dashboards are available for a long periods of time (3 months at the moment) and are available from different parts of world to developers, scientists and engineers.
	
	\section{ACKNOWLEDGEMENTS}
	This work has been supported by Italian Government (MEF - Ministero dell'Economia e delle Finanze, MIUR - Ministero dell'Istruzione, dell'Università e della Ricerca).
	
	%
	% only for "biblatex"
	%
	\ifboolexpr{bool{jacowbiblatex}}%
	{\printbibliography}%
	{%
		% "biblatex" is not used, go the "manual" way
		
		%\begin{thebibliography}{99}   % Use for  10-99  references
		\begin{thebibliography}{9} % Use for 1-9 references
			
			\bibitem{SKA-CICD}
			``Ci-cd practices at ska,'' International Society for Optics and Photonics,
			SPIE (2022).
			
			\bibitem{gitlab}
			``Gitlab.'' \url{https://about.gitlab.com/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{elasticsearch}
			``Elasticsearch.'' \url{https://www.elastic.co/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{ceph}
			``Ceph storage.'' \url{https://ceph.io/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{nexus}
			``Nexus.'' \url{https://www.sonatype.com/products/sonatype-nexus-repository}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{prometheus}
			``Prometheus.'' \url{https://prometheus.io}.
			\newblock (Accessed: 1 September 2023).
			
			\bibitem{tsdb}
			``Tsdb format.''
			\url{https://github.com/prometheus/prometheus/tree/release-2.22/tsdb/docs/format}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{node_exporter}
			``Node exporter.'' \url{https://github.com/prometheus/node_exporter}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{kubernetes}
			``Kubernetes.'' \url{https://kubernetes.io/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{kube-state-metrics}
			``kube-state-metrics.'' \url{https://github.com/kubernetes/kube-state-metrics}.
			\newblock (Accessed: 31 August 2023).

			\bibitem{openstack}
			``OpenStack.'' \url{https://www.openstack.org/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{ansible}
			``Ansible.'' \url{https://www.ansible.com/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{kubernetes-mixin}
			``Prometheus monitoring mixin for kubernetes.''
			\url{https://github.com/kubernetes-monitoring/kubernetes-mixin}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{slack}
			``Slack.'' \url{https://slack.com}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{thanos}
			``Thanos.'' \url{https://thanos.io}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{grafana}
			``Grafana.'' \url{https://grafana.com/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{node-exporter-full}
			``Node exporter full.'' \url{https://grafana.com/grafana/dashboards/1860-node-exporter-full/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{elasticsearch-dashboard}
			``Elasticsearch dashboard.'' \url{https://grafana.com/grafana/dashboards/878-elasticsearch-dashboard/}.
			\newblock (Accessed: 31 August 2023).
			
			\bibitem{docker-dashboard}
			``Docker and system monitoring.''
			\url{https://grafana.com/grafana/dashboards/893-main/}.
			\newblock (Accessed: 31 August 2023).
			
		\end{thebibliography}
	} % end \ifboolexpr
	%
	% for use as JACoW template the inclusion of the ANNEX parts have been commented out
	% to generate the complete documentation please remove the "%" of the next two commands
	%
	%%%\newpage
	
	%%%\include{annexes-A4}
	
\end{document}